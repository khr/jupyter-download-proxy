import setuptools

setuptools.setup(
    name="jupyter-download-proxy",
    version='0.0.1',
    url="https://gitlab.mpcdf.mpg.de/khr/jupyter-download-proxy",
    author="Klaus Reuter",
    description="klaus.reuter@mpcdf.mpg.de",
    packages=setuptools.find_packages(),
	keywords=['Jupyter'],
	classifiers=['Framework :: Jupyter'],
    install_requires=[
        'jupyter-server-proxy'
    ],
    entry_points={
        'jupyter_serverproxy_servers': [
            'download = jupyter_download_proxy:setup_download_proxy',
        ]
    },
    package_data={
        'jupyter_download_proxy': ['icons/*'],
    },
)
