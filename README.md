# jupyter-download-proxy : A proxy that enables a simple http server on top of JupyterLab/Binder

This proxy runs the simple internal Python http server to expose the working
directory for downloading files. The proxy was initially created as a workaround
since the Streamlit-internal download button uses buffering in memory which
breaks, starting from a certain file size.

Warning: Once installed and enabled, the full working directory of JupyterLab
is accessible via http. Only use in zero-security environments such as Binder!

A usage example, the Streamlit-based web application GlycoSHIELD, is available at   
https://gitlab.mpcdf.mpg.de/dioscuri-biophysics/glycoshield-md

The package is provided as-is, see 'LICENSE'.

This package was adapted from   
https://jupyter-server-proxy.readthedocs.io/en/latest/
