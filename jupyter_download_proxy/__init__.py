"""
Return config on servers to start web services from JupyterLab,
in this case the basic Python http server.  This functionality
is useful to create download links for (large) files from
Streamlit web apps or Jupyter notebooks, sidestepping certain
size limitations.

Warning: Only use this in a Binder environment, not in a local or critical environment!

See https://jupyter-server-proxy.readthedocs.io/en/latest/server-process.html
for more information.
"""

import os


def setup_download_proxy():
    """
    Proxy wrapper to run the basic Python-internal http server from the current working directory.

    Warning: Only use this in a Binder environment, not in a local or critical environment!
    """
    return {
        'command': [
            "python3", "-m", "http.server", "{port}"
        ],
        'environment': {},
        'timeout': 30.0,
        'launcher_entry': {
            'title': 'download',
            'icon_path': os.path.join(os.path.dirname(os.path.abspath(__file__)), 'icons', 'generic-download-icon.svg'),
        }
    }
